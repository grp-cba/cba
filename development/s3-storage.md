## General

- Pairs of keys
    - User name = Access Key ID (AWS) = Access Key (MinIO)
    - Password = Access Key (AWS) = Secret Key (MinIO)
    - One pair of keys can be for one bucket or for several buckets
- Bucket
    - Container of many objects

## Getting started

- send a mail to itsupport@embl.de
	- "I would like to use the s3 storage, could please create an account for me and a bucket with the name "MY_PROJECT_NAME".


## EMBL MinIO Web interface

- https://console.s3.embl.de/login
- credentials:
    - User name = Access Key ID (AWS) = Access Key ((MinIO)
    - Password = Access Key (AWS) = Secret Key (MinIO)
- If you have a minio client this information may be stored here: `cat ~/.mc/config.json`
  - This file would be also the place where you could store new pairs of keys
- The file may be on your local computer or on the EMBL home
    - For EMBL Home
        - Log in to login node
        - `module load mc`
        - `cat ~/.mc/config.json`
- Add a new bucket: `https://console.s3.embl.de/add-bucket`

## MinIO mc command line client

### Start

- shh USER@login.cluster.embl.de
- module load mc

### Documentation

https://docs.min.io/docs/minio-client-complete-guide.html


### Authentication

```
-bash-4.2$ cat ~/.mc/config.json
{
	"version": "10",
	"aliases": {
		"embl": {
			"url": "https://s3.embl.de",
			"accessKey": "cbb-bigdata",
			"secretKey": "123",
			"api": "s3v4",
			"path": "auto"
		}
	}
}
```

This means you can access the "embl" alias, e.g. like this:

```
-bash-4.2$ mc ls embl
[2021-10-08 12:20:51 CEST]     0B platybrowser/
```

### List content

List all buckets of the `embl` user (s.a.):
`mc ls embl`

List objects within the `comulis` bucket of the `embl` user:
`mc ls embl/comulis

List all objects in the comulis bucket recursively:
`mc ls -r embl/comulis`

### Print the content of a file

- Using the `embl` alias print the content of `project.json` in the comulis `bucket`: `mc cat embl/comulis/project.json`

### Copy a file to a bucket

Assuming that the file test.txt exist in the current folder and that the embl user has a bucket called comulis:

`mc cp test.txt embl/comulis`

### Copy many files to a bucket

- Create interactive job (180 minutes): `srun -c 8 --mem 32000 -t 180:00 --pty bash -l`
- `module load mc`
- Recursively copy the content of `data` directory for alias `embl` into bucket `comulis`: `mc cp -r data/ embl/comulis`

### Remove all files from a bucket

Using the `embl` alias, remove all objects from the bucket `comulis`.

`mc rm --recursive embl/comulis`

`mc rm --recursive --force embl/comulis`


### Add a service account

Add service account for `embl` user/alias on the `cbb-bigdata` account (this will return the pair of keys).
This feels a bit weird, because the `embl` alias already contains the `cbb-bigdata` account name, so I am not sure why one has to add it a second time. Maybe the point is that the `embl` alias itself could be a service account...

`mc admin user svcacct add embl cbb-bigdata`

Returns pair of keys, which have the same rights as the `embl` alias.


#### Add a service account with policy

- create a policy JSON file using: https://awspolicygen.s3.amazonaws.com/policygen.html

Below policy file restricts all actions to the `comulis` bucket. One could restrict the actions even more.
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Principal": {
       "AWS": [
        "*"
       ]
      },
      "Resource": [
    "arn:aws:s3:::comulis/*"
      ],
      "Sid": ""
    }
  ]
}
```

Then use above policy.json (which restricts read and write access to the `comulis` bucket)  for the service account access key `"UYP3FNN3V5F0P86DR2O3"` of the `embl` account alias.

`mc admin user svcacct set embl "UYP3FNN3V5F0P86DR2O3" --policy policy.json`




### Create credentials alias

To add another alias to `~/.mc/config.json` use:

`mc alias set <ALIAS> <YOUR-S3-ENDPOINT> [YOUR-ACCESS-KEY] [YOUR-SECRET-KEY]`

### Create a new bucket

Normally `mc mb embl/comulis` would work to create the new bucket `comulis` for the `embl` alias, but currently this is not enabled at EMBL.


## Java

### Example code

```
package develop;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import ij.gui.GenericDialog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class ExploreAWSS3API
{
	public static void main( String[] args ) throws IOException
	{
		// Get credentials
		final GenericDialog dialog = new GenericDialog( "" );
		dialog.addStringField( "accessKey", "" );
		dialog.addStringField( "secretKey", "" );
		dialog.showDialog();
		if (!dialog.wasCanceled());
		final String accessKey = dialog.getNextString();
		final String secretKey = dialog.getNextString();

		// Create AWS service
		final String serviceEndpoint = "https://s3.embl.de";
		final String signingRegion = "us-west-2";

		final AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration( serviceEndpoint, signingRegion );

		final BasicAWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		final AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
		final AmazonS3 s3 = AmazonS3ClientBuilder
				.standard()
				.withCredentials(credentialsProvider)
				.withEndpointConfiguration(endpointConfiguration)
				.withPathStyleAccessEnabled(true)
				.build();

		// List buckets
		List<Bucket> buckets = s3.listBuckets();
		System.out.println("Your buckets are:");
		for (Bucket bucket : buckets) {
			System.out.println("* " + bucket.getName());
		}

		// List objects in bucket
		ListObjectsV2Request request = new ListObjectsV2Request()
				.withBucketName("comulis")
				.withPrefix("")
				.withDelimiter("/");
		final ListObjectsV2Result result = s3.listObjectsV2( request );
		final List< S3ObjectSummary > objectSummaries = result.getObjectSummaries();

		for (S3ObjectSummary objectSummary : objectSummaries) {
			System.out.printf(" - %s (size: %d)\n", objectSummary.getKey(), objectSummary.getSize());
		}

		// Download object
		final S3Object s3Object = s3.getObject( new GetObjectRequest( "comulis", "test.txt" ) );
		System.out.println("Content-Type: " + s3Object.getObjectMetadata().getContentType());
		System.out.println("Content: ");
		displayTextInputStream(s3Object.getObjectContent());
	}

	private static void displayTextInputStream( InputStream input) throws IOException
	{
		// Read the text input stream one line at a time and display each line.
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String line = null;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
		System.out.println();
	}
}
```
