## Create jar files in IntelliJ for Fiji

  - [Working with IntelliJ artifacts](https://www.jetbrains.com/help/idea/working-with-artifacts.html)


### Steps

1. Make sure that in  `File > Project Structure` under `Project Settings > Project` tab, you have right SDK. For me “Coretto-1.8 java version 1.8.0_312” works.
2. Also select the same in `Project Structure` under `Platform Settings > SDKs` tab.
3. Check if the project builds successfully.
4. Now in `Project Structure`, go to:  `Project Settings > Artifacts` tab and add using  `+` button select `jar> from modules with dependencies…`.
5. The following window will appear: ![Artifacts](../screenshots/intellij_window1.png)
   select the module you want to package in `Module` drop down and your main java class in the using browse folder in the `Main Class` box. Then press `OK`.
6. In the `Output Layout` tab, select `plugin-name compile output` element and remove the rest: ![Output Layout](../screenshots/intellij_window2.png)
   to remove elements, `-` button can be used with multiple selection. And then press `OK`.
7. Do `Build > Build Artifacts...` and select `plugin-name:jar > Build`.

   The resulting `plugin-name.jar` file will appear in folder `out > artifacts > plugin_name_jar`

### Note

If you do not do step 6 selection and use all elements in the `Output Layout`, then with build artifacts, it gives you a large `.jar` file (> 200 MB in my case). If step 6 is followed, it gives you a `.jar` file with smaller size (<100 KBs).
