# Install CellProfiler Singularity container on HPC

## Resources

- https://github.com/pharmbio/cellprofiler-docker#using-singularity
- https://forum.image.sc/t/cellprofiler-singularity/27720/3
- https://git.embl.de/ysun/container-recipes/-/blob/master/cellprofiler4/Singularity.u20
- https://github.com/CellProfiler/CellProfiler/wiki/Ubuntu-20.04

## Example for version 4.1.3

- `ssh tischer@login.cluster.embl.de` # log into cluster
- `srun -t 100:00 --mem 16000 --pty bash -l` # get interactive node for 100 minutes
- `cd /g/almf/software/`
- `mkdir CellProfiler-4.1.3-Singularity`
- `cd CellProfiler-4.1.3-Singularity`
- `singularity pull cellprofiler.v4.1.3.sif docker://pharmbio/cellprofiler:v4.1.3` # https://github.com/pharmbio/cellprofiler-docker#using-singularity
- `mv cellprofiler.v4.1.3.sif cp-4.1.3.sif`
- `vi cp-4.1.3` # create an executable that runs the singularity container
  - add below lines:
  - `#!/bin/bash`
  - `singularity exec -B /g:/g /g/almf/software/CellProfiler-4.1.3-Singularity/cp-4.1.3.sif cellprofiler "$@"`
- `chmod +x cp-4.1.3`
