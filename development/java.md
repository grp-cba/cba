## check versions

`/usr/libexec/java_home -V`

## set version temporarily

For example:
`export JAVA_HOME=$(/usr/libexec/java_home -v 17)`

## set permanently 

### MacOS

`vi ~/.bash_profile`

and add, e.g.

`export JAVA_HOME=$(/usr/libexec/java_home -v 17)`
