## Fiji developer resources
  
  - [Examples of scripting Fiji in different languages](https://imagej.nih.gov/ij/developer/)
  - [Develop jython in Eclipse](https://haesleinhuepf.github.io/run_jython_scripts_from_ide/)
  - [Use IJ-Ops](https://github.com/imagej/tutorials/tree/master/howtos/src/main/java/howto/ops)
  - [Run Fiji scripts from the CLI](https://imagej.net/scripting/headless)
  - [Fiji troubleshooting](https://imagej.net/learn/troubleshooting#If_ImageJ_freezes_or_hangs)

## Create and maintain a Fiji update site

### Create a new update site

1. Get a user account for ImageJ (TODO: how?)
1. Request the creation of an update site [here](https://forum.image.sc/t/requests-for-creating-imagej-update-sites/40051/214).

### Register the new update site for upload in Fiji

1. Download a [fresh Fiji](https://fiji.sc/)
1. Rename it, e.g.: `Fiji-MyUpdateSite00.app`; you will use this Fiji only to upload jar files to your update site.
1. Start Fiji
1. Update it once <kbd>Help > Update...</kbd>
1. Close Fiji
1. Start Fiji
1. <kbd>Help > Update...</kbd> 
1. <kbd>Manage Update Sites</kbd>
1. <kbd>Add update site</kbd>
1. Name: `MyUpdateSite00`; URL: `https://sites.imagej.net/MyUpdateSite00`; host: `webdav:YourUserName`
1. Close Fiji

### Add the update site to the list of update sites

This is necessary for the update site to appear in the list of update sites that the users see when they <kbd>Manage Update Sites</kbd> in Fiji.

1. Clone `git@github.com:imagej/list-of-update-sites.git`
1. Or if you have it already `pull` again the `master` branch to make sure you are up to date!
1. `vim sites.yml`

Add your update site at the correct alphabetical position, e.g.

```
  - name: "Microglia-Morphometry"
    id: "Microglia-Morphometry"
    url: "https://sites.imagej.net/Microglia-Morphometry/"
    description: >-
      [Microglia-Morphometry](https://github.com/embl-cba/microglia-morphometry#microglia-morphometry)
      is a tool for seim-automated segmentation, tracking and morphometric analysis of 2D microglia time-lapse images.
      - "[Christian Tischer](mailto:christian.tischer@embl.de)"
```

Create a PR and have it merged, e.g. by `etadobson`


### Upload jars to your update site

1. Make sure your Fiji is closed
1. Add the jars for your update site to Fiji (this may be new jars or replacing already existing jars by newer versions)
1. This can either be done *manually* by moving them into the jars folder...
1. ...or automatically, e.g. `mvn clean install -Dscijava.app.directory=/Users/tischer/Desktop/Fiji-MyUpdateSite00.app`
1. Start Fiji
1. Update
1. Close Fiji
1. Start Fiji
1. <kbd>Help > Update...</kbd> 
1. For adding new jars: View Options: `View local-only files`
1. In the Status/Action column change to `Upload it`.
1. Upload only jars that are really relevant!





