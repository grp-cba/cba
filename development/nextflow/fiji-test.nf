#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.fiji = '/Users/tischer/Desktop/Fiji/Fiji.app/Contents/MacOS/ImageJ-macosx --headless --run'
params.scriptDir = '/Users/tischer/Documents/cba/development/nextflow'


process createImages {
  debug true
  output:
    path 'image_*'

  shell:
  '''
  !{params.fiji} !{params.scriptDir}/create_images.groovy "outputDir='${PWD}'"
  '''
}

process blurImages {
  debug true

  input:
    file x

  shell:
  '''
  !{params.fiji} !{params.scriptDir}/blur_image.groovy "inputFile='${PWD}/!{x}',outputDir='${PWD}'"
  '''
}

workflow {
  createImages | flatten | blurImages
}



