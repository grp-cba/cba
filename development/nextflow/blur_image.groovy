import ij.IJ;
import java.io.*;
import org.apache.commons.io.*;
import java.lang.*;

#@ File (label = "Input image") inputFile
#@ File (label = "Output directory", style="directory") outputDir

IJ.log("# Blur image");
IJ.log("inputFile: " + inputFile)
IJ.log("outputDir: " + outputDir)
IJ.log("currentDir: " + IJ.getDirectory("current"))


// open
imp = IJ.openImage(inputFile.toString())

// process
IJ.run(imp, "Gaussian Blur...", "sigma=20")

// save
outName = FilenameUtils.removeExtension(inputFile.getName()) + "_blur.tif"
outPath = outputDir.toString() + File.separator + outName
IJ.log("Saving " + outPath)
IJ.saveAs(imp, "Tiff", outPath)

// close Fiji
System.exit(0)
