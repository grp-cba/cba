import ij.IJ;
import java.io.*;

#@ File (label = "Directory", style="directory") outputDir

IJ.log("# Create images");
IJ.log("outputDir: " + outputDir);

imp = IJ.openImage("http://imagej.nih.gov/ij/images/blobs.gif");
for(i=0;i<3;++i)
{
	outPath = outputDir.toString() + File.separator + "image_"+i+".tif"
	IJ.log("Saving " + outPath)
	IJ.saveAs(imp, "Tiff", outPath);
}
