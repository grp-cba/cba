#!/usr/bin/env nextflow
nextflow.enable.dsl=2

process init {
  output:
    path 'test.txt'
  
  script:
  '''
  echo hello > test.txt
  '''
}

process echoPWD {
  debug true
  
  input:
    file x
  output:
    path '*.txt'

  shell:
  '''
  echo "with / $PWD/!{x}"
  echo "with : ${PWD}:!{x} "
  '''
}

workflow {
  init | flatten | echoPWD
}

