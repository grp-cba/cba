# CBA conda environments

## Activate conda

```
source /g/cba/miniconda3/etc/profile.d/conda.sh
```

## Debugging


Check whether there are other conda environments interfering:

```
conda config --show
```

- `envs_dirs`
- `pkgs_dirs`

## Install new environments

It is important to give the full path where to create the env:

```
source /g/cba/miniconda3/etc/profile.d/conda.sh 
conda create -c conda-forge mobie_utils -p /g/cba/miniconda3/envs/mobie
```

To be sure that all CBA group members can continue to work on those enviroments, please grant everyone rights once you are done:

```
chmod -R g+rwx /g/cba/miniconda3/
```

 
