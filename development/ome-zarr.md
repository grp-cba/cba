## Recursively modify all .zarray files in a folder

replaces `: "0"` with `: 0`:

`find . -path './*/*/.zarray' -exec grep -i ': "0"' -l {} \; -exec sed -i 's/: "0"/: 0/g' {} \;`

## Change the data set name on S3

Constantin: In general, changes to the metadata are pretty easy:
- modify a local copy of .zattrs (you can for example get it by reversing the command below)
- `mc cp .zattrs embl/i2k-2020/platy-raw.ome.zarr/.zattrs`

## Fetch version from S3

- install: https://stedolan.github.io/jq/
- `curl -sq https://s3.embl.de/i2k-2020/platy-raw.ome.zarr/.zattrs | jq '.["multiscales"][0]["version"]'`



