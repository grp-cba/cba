## Launching a Jupyter desktop

From intranet: 

- test deployment: [https://jupyterhub.test.embl.de/](https://jupyterhub.test.embl.de/) 
  - build from [images](https://git.embl.de/grp-jupyterhub/jupyterhub-images) ending on `-1.3`
- production deployment: [https://jupyterhub.embl.de/](https://jupyterhub.embl.de/) 


## Images on github

The repository [https://git.embl.de/grp-jupyterhub/jupyterhub-images](https://git.embl.de/grp-jupyterhub/jupyterhub-images) contains Dockerfile recipes for building various virtual desktops.

### Images maintained by CBA

- [napari-gpu-desktop](https://git.embl.de/grp-jupyterhub/jupyterhub-images/-/blob/master/02-napari-gpu-desktop/Dockerfile)

## Docker

- [best practices creating a dockerfile](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [one or multiple RUN](https://stackoverflow.com/questions/39223249/multiple-run-vs-single-chained-run-in-dockerfile-which-is-better)

### Dockerfile tipps and tricks

- Use `python -m pip install` and not just `pip install` (https://snarky.ca/why-you-should-use-python-m-pip/).
- Probably better to use `RUN conda run --name package_name do_stuff` instead of `RUN conda active package_name && do_stuff`
  - JK: "Actually conda activate may not work by itself because it needs a shell to do some of its work like conda init so you probably need conda init bash somewhere before conda activate."


### Locally build and modify a Dockerfile

```
git clone git@git.embl.de:grp-jupyterhub/jupyterhub-images.git 
rm -rf ~/.docker/config.json ## otherwise the next command may not work...
docker login git.embl.de:4567 # to download an image from an repository
cd jupyterhub-images/02-napari-gpu-desktop
git pull # make sure to be up to date
docker build .
vi Dockerfile # change stuff
docker build . # rebuild
```

### Check image

- `docker images`
- `docker history [OPTIONS] IMAGE`


### Check docker disk space

- `docker system df`



### Clean up docker images and cache

- remove really everything: `docker system prune -a` 
- more careful options: `docker image prune`, `docker builder prune`


#### Introspect the built container

Regarding your question of how you can do a local inspection of a container meant to be run on jupyterhub, you could for instance use: 
```
docker run -it --entrypoint /bin/bash git.embl.de:4567/grp-jupyterhub/jupyterhub-images/datascience-1.3
``` 
...that will give you an interactive bash shell.


### Install ilastik

```
ARG ilastik_binary
WORKDIR /opt
RUN wget https://files.ilastik.org/ilastik-1.3.3post3-Linux.tar.bz2 && \
  tar xjf ilastik-1.*-Linux.tar.bz2 && \
  rm ilastik-1.*-Linux.tar.bz2 && \
  chmod -R a+rwx /opt/Fiji.app && \
  echo $'#!/usr/bin/env sh\n/opt/ilastik-1.*-Linux/run_ilastik.sh $@' >> /usr/local/bin/ilastik && \
  chmod +x /usr/local/bin/ilastik
```

### Install Fiji

- `/opt` is the place to install software
- The software should be started from `/usr/local/bin`, thus we add script there that launches the Fiji installed in `/op`
- The last `--update update` is important, because the commands before in fact only "add" the update sites but do not install them.

```
WORKDIR /opt
RUN wget https://downloads.imagej.net/fiji/latest/fiji-linux64.zip && \
  unzip fiji-linux64.zip && \
  rm fiji-linux64.zip && \
  chmod -R a+rwx /opt/Fiji.app && \
  echo $'#!/usr/bin/env sh\n/opt/Fiji.app/ImageJ-linux64 $@' >> /usr/local/bin/fiji && \
  chmod +x /usr/local/bin/fiji && \
  Fiji.app/ImageJ-linux64 --headless --update add-update-site IJPB-plugins https://sites.imagej.net/IJPB-plugins/ && \
  Fiji.app/ImageJ-linux64 --headless --update add-update-site BigDataProcessor https://sites.imagej.net/BigDataProcessor/ && \
  Fiji.app/ImageJ-linux64 --headless --update add-update-site MoBIE-beta https://sites.imagej.net/MoBIE-beta/ && \
  Fiji.app/ImageJ-linux64 --headless --update add-update-site CSBDeep https://sites.imagej.net/CSBDeep/ && \
  Fiji.app/ImageJ-linux64 --headless --update add-update-site StarDist https://sites.imagej.net/StarDist/ && \
  Fiji.app/ImageJ-linux64 --headless --update update
```

#### Install tensorflow for CSBDeep

I abandoned this for now, waiting for a better deep learning integration in Fiji.

- `nvidia-smi` tells the CUDA version (last time I checked: `11.2`)
- see also: https://forum.image.sc/t/fiji-install-tensorflow-command-line/64138
- here is more information: https://github.com/deepimagej/deepimagej-plugin/wiki/GPU-connection
- Manually: In Fiji [ Edit > Options > Tensorflow ]
  - If nothing is installed it says: `No Tensorflow library found in /opt/Fiji.app/lib`.
  - In fact, in `/opt/Fiji.app/lib/linux64` there only is `libblosc.so`
  - When selecting `TF 1.15 GPU (CUDA 10.0 CuDNN > 7.4.1)` it says:

```
[INFO] Downloading https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow_jni-gpu-linux-x86_64-1.15.0.tar.gz to /opt/Fiji.app/downloads/libtensorflow_jni-gpu-linux-x86_64-1.15.0.tar.gz
[INFO] Installing TF 1.15.0 GPU (CUDA 10.0, CuDNN >= 7.4.1)
[INFO] Unpacking /opt/Fiji.app/downloads/libtensorflow_jni-gpu-linux-x86_64-1.15.0.tar.gz to /opt/Fiji.app/update/lib/linux64
[INFO] Writing /opt/Fiji.app/update/lib/linux64/./THIRD_PARTY_TF_JNI_LICENSES
[INFO] Writing /opt/Fiji.app/update/lib/linux64/./LICENSE
[INFO] Writing /opt/Fiji.app/update/lib/linux64/./libtensorflow_jni.so
[INFO] Writing /opt/Fiji.app/update/lib/linux64/./libtensorflow_framework.so.1.15.0
[INFO] Creating symbolic link: /opt/Fiji.app/update/lib/linux64/./libtensorflow_framework.so -> /opt/Fiji.app/lib/linux64/libtensorflow_framework.so.1
[INFO] Creating symbolic link: /opt/Fiji.app/update/lib/linux64/./libtensorflow_framework.so.1 -> /opt/Fiji.app/lib/linux64/libtensorflow_framework.so.1.15.0
```

After which:

```
(base) tischer@jupyter-tischer:/home/tischer$ ls /opt/Fiji.app/update/lib/linux64/
libtensorflow_framework.so         libtensorflow_jni.so
libtensorflow_framework.so.1       LICENSE
libtensorflow_framework.so.1.15.0  THIRD_PARTY_TF_JNI_LICENSES
```

However the files seem missing:

```
(base) tischer@jupyter-tischer:/home/tischer$ ls -la /opt/Fiji.app/lib/linux64/
total 936
drwxrwxrwx. 1 root    root         32 Mar 27 13:57 .
drwxrwxrwx. 1 root    root         21 Mar 24 18:40 ..
-rwxrwxrwx. 1 root    root     951032 Oct 29  2020 libblosc.so
-rw-r--r--. 1 tischer pepperko     32 Mar 27 13:57 .tensorflowversion
```

However after a restart of Fiji:

```
(base) tischer@jupyter-tischer:/home/tischer$ ls /opt/Fiji.app/lib/linux64
libblosc.so                        libtensorflow_jni.so
libtensorflow_framework.so         LICENSE
libtensorflow_framework.so.1       THIRD_PARTY_TF_JNI_LICENSES
libtensorflow_framework.so.1.15.0
```
   
### Monitor GPU usage

- `watch -n 1 nvidia-smi >> log.txt`


### Troubleshooting

#### docker login

Error:

```
[+] Building 0.2s (3/3) FINISHED                                                
 => [internal] load build definition from Dockerfile                       0.0s
 => => transferring dockerfile: 731B                                       0.0s
 => [internal] load .dockerignore                                          0.0s
 => => transferring context: 2B                                            0.0s
 => ERROR [internal] load metadata for git.embl.de:4567/grp-jupyterhub/ju  0.1s
------
 > [internal] load metadata for git.embl.de:4567/grp-jupyterhub/jupyterhub-images/jupyter-desktop-server-1.3-noldap:latest:
------
failed to solve with frontend dockerfile.v0: failed to create LLB definition: rpc error: code = Unknown desc = error getting credentials - err: exit status 1, out: `error getting credentials - err: exit status 1, out: `The user name or passphrase you entered is not correct.``
```

Solution: `docker login git.embl.de:4567`

Doing `docker login` you may encounter this error:

```
Error saving credentials: error storing credentials - err: exit status 1, out: `error getting credentials - err: exit status 1, out: `The user name or passphrase you entered is not correct.``
```

Potential solution for Mac: https://github.com/aws/aws-cli/issues/3264

#### docker daemon

Error:

```
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
```

Solution: You need to ensure that docker is running. On a Mac this is achieved by starting the `Docker.App`

