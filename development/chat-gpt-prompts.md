# Individual prompts

## Nuclear shape analysis

### Nuclear shape analysis 1

Those are two fluorescence microscopy images of nuclei of tissue culture cells. Could you please segment the nuclei in both images and analyze whether there are any difference in their shapes between the two images?

### Nuclear shape analysis 2

Those are two fluorescence microscopy images of nuclei of tissue culture cells. Could you please segment the nuclei in both images and analyze whether there are any difference in their shapes between the two images?

Please take into account the following when doing the segmentation:
- Please show the raw images
- Please show the images after each processing step (e.g., filtering, thresholding, connected component labelling, etc.)
- The intensity of nuclei even within one image can vary quite a lot, please choose a thresholding strategy that can segment relatively dim and bright objects at the same time 
- The images are not very noisy, thus no smoothing is required
- The nuclei have a width between 20 to 60 pixels, you may use this information for morphological filtering to clean up the segmentation, but please do not remove nuclei based on on this

Specific hints:
- When using the "threshold_local" method, please leave the offset at 0


Please do all steps until the final shape analysis and statistical comparison in one go.

### Nuclear shape analysis: ground truth

Hi, here are two images, the one with "labels" in the name contains a label mask of the nuclei in the other image, which is a fluorescence microscopy image. 

Please compare all thresholding methods of skimage on the fluorescence image in terms of how well they match the provided label ground truth! Please exclude the multi_otsu_threshold method as this works differently.

Please for all methods show in a tabular form that makes it easy to compare:
- The evaluation metrics, ideally also add some that work on the labels and not only on the binary imaage
- The object counts (absolute and relative to the ground truth)

Please also show the respective 
- binary masks (incl. ground truth)
- label masks (incl. ground truth)

When showing the images, please consider that you have limited memory and thus show them all together in some small version. When showing all images together please consider laying them out such that the titles of the images can be well read and don't overlap with each other. 

Then please in addition, show the two best segmentations in full size. 



# Pre-prompt

We are going to ask to help us writing mainly python based bioimage analysis code. Please note a few things upfront that are important to consider.

- If possible please use the skimage library for image analysis tasks. If you consider some other library to be better suited for some specific task please let us know.
- Please write the python scripts such that they can be executed from the command line, typically having a path to an image as one of its inputs; please use argparse for this purpose.
- Please always save segmentation results as label mask TIFF files, using the filename of the input image, but, e.g. if nuclei have been segmented then using “_nuclei_labels.tiff” as the new ending. If other objects have been segmented please replace "nuclei" by the corresponding name.
- Each python script should typically only analyse one image (data set).
- For batch analysis of many images please use nextflow, using the DSL 2 specification. Regarding nextflow please consider: 
  - DSL 2 is using “path” instead of “file”  
  - The input data should be fetched in the workflow block, e.g. using Channel.fromPath
  - We prefer using conda environments for specifiying the context in which the python scripts should run. Please specify them for each step in the workflow. 
- For object segmentation, typically there is a parameter that specifies the average radius of one object in pixel units. Please expose this parameter in the python scripts and also use it in the segmentation, e.g. to remove small objects or to perform morphological filtering operations.


If asked for a specific solution using the above guidelines, you do not need to repeat the guidelines. 
Please simply produce the python and nextflow scripts with appropriate comments.

OK, are you ready to go? Please just confirm that you understood, do not yet generate an example script.


## Prompts

Great! Using the above guidelines, could you please develop a solution that segments nuclei in several 2D grayscale fluorescence microscopy images that stored in a folder on a computer?  

# Chat histories

- [Nuclear segmentation](https://chat.openai.com/c/ab29e826-1ed4-4fc8-a69d-2ac41d63c3b9)



