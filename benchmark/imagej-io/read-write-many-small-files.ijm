// parameters

numIterations = 100;
directory = getDirectory("Choose a Directory");
// directory = "/Users/tischer/Desktop/tmp";

// code
run("Close All");
setBatchMode( true );

// prepare test image
newImage("image", "8-bit black", 500, 500, 1);
makeOval(122, 106, 252, 285);
setForegroundColor(255, 255, 255);
run("Fill", "slice");
run("Select None");
run("Gaussian Blur...", "sigma=50");
run("RGB Color");
inputImagePath = directory + File.separator + "input-image.tif";
saveAs("Tiff", inputImagePath);
wait(500);

// run benchmarking
start = getTime(); 

for (i = 0; i < numIterations; i++) 
{
	run("Close All");
	open(inputImagePath);
	outputImagePath = directory + File.separator + "output-image-" + IJ.pad(i, 6) + ".tif" ;
	saveAs("Tiff", outputImagePath);
}

print("Saved " + numIterations + " files in " + (getTime()-start) + " ms.");   