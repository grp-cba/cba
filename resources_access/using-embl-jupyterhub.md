# Image analysis on EMBL's Jupyter desktop

## Log in

- Open a web browser and go to [jupyterhub](http://jupyterhub.embl.de/)
- Use your EMBL credentials to log in
- In `Server Options`, select `Image Analysis GPU Desktop` and press `Start`
- A linux desktop will appear in your browser
- Open a terminal window:
  - either go to `Applications > Terminal Emulator`
  - or click on the icon on the bottom of the screen: ![terminal emulator](screenshots/terminal-emulator-jupyterhub.png)


## Access data on your network share

To check whether you have access, please open a terminal (s.a.) and type:

`ls /g/SERVERNAME`

For example, if you are a member of the Cuylen lab, this should print the folders on the`cuylen` network share:

`ls /g/cuylen`

If that does not work, please contact us (s.b.).


## Launch preinstalled software

Please open a terminal (s.a.) and type:

- `fiji`
- `cellpose`
- `ilastik`
- `cellprofiler-4.2.6`
- `napari`
- `nextflow`
- `mobie-python`
- `mobie`

For developers, please see [here](https://git.embl.de/grp-jupyterhub/jupyterhub-images/-/blob/master/02-napari-gpu-desktop/Dockerfile) for the current installation recipes.

## Contact us

If you have any questions, please contact us:

- https://chat.embl.org/embl/channels/image-analysis
