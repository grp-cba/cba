### Mounting Data from GroupShared directory to Ubuntu

1. **Check if the directory is accessible:**  
   Try navigating to the directory (replace `/path/to/directory` with the actual path):
   ```bash
   cd /path/to/directory
   ```
If this does not work, proceed with the steps below.

2. **Install CIFS Utilities:**
    Open a terminal and install CIFS (Common Internet File System) utilities:
    ```bash
    sudo apt-get install cifs-utils
    ```
3. **Create a Local Mount Point:**
    Make a directory to serve as your mount point:
    ```bash
    mkdir -p ~/mount-point
    ```
4. **Mount the Shared Directory:**
    Use the following command to mount the network drive:
    ```bash
    sudo mount -t cifs -o username=USERNAME,uid=$(id -u),gid=$(id -g) //network.path/to/shared/directory ~/mount-point
    ```
    Replace USERNAME with your username and //network.path/to/shared/directory with the actual network path.

5. **Verify the Mounting:**
    Check if the mount was successful:
    ```bash
    ls ~/mount-point
    ```
6. **Create a Personal Directory:**
    Once the mount is successful, create a directory for your files:    
    ```bash
    mkdir ~/mount-point/YOUR_DIRECTORY_NAME
    ```
    Replace YOUR_DIRECTORY_NAME with your chosen name.
        Important: Use a simple directory name (e.g., "einstein") without spaces or special characters.

This should set up the mount so you can access shared files directly from Ubuntu.