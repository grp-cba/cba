## How to access `/cba/exchange`

### Windows

- Make sure you are in the EMBL intranet 
- Open a "Windows Explorer"
- Click on "This PC"
- In the menu select "Computer" 
- Then click on "Map network drive"
- Enter: `\\cba\cba\exchange` (if it does not work, try `\\cba.embl.de\cba\exchange`)
- you can log in with your EMBL Heidelberg account:
- embl\USERNAME
- PASSWORD
- create a folder for you, e.g., if your last name is `einstein`: `\\cba\cba\exchange\einstein` (NO spaces and other funny characters!)

### MacOs

- Make sure you are in the EMBL intranet 
- Within Finder, "Go > Connect to Server" and enter
- `cifs://USERNAME:*@cba.embl.de/cba/exchange`
- replacing `USERNAME` by your EMBL Heidelberg username
- In Finder, you can now "Go > Go to Folder" and enter `/Volumes/cba/exchange`
- create a folder for you, e.g., if your last name is `einstein`: `/Volumes/cba/exchange/einstein` (NO spaces and other funny characters!)

### Linux

- Try `cd /g/cba/exchange`, if that does not work, follow the below instructions
- Open a terminal window and install cifs and keyutils (e.g. `apt-get install cifs-utils`) 
- `mkdir -p ~/cba-exchange`
- `sudo mount -t cifs -o username=USERNAME,uid=$(id -u),gid=$(id -g) //cba.embl.de/cba/exchange ~/cba-exchange`
- replace `USERNAME` by your EMBL Heidelberg username
- now you should be able to `ls ~/cba-exchange` 
- and `mkdir ~/cba-exchange/USERNAME`
- replace `USERNAME` by your embl username
- create a folder for you, e.g., if your last name is `einstein`: `/g/cba/exchange/einstein` (NO spaces and other funny characters!)


