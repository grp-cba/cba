## EMBL Cluster graphics session with TurboNVC
- _IMPORTANT_: Before processing make sure that you have `TurboVNC` installed on your computer.
  - Fetch an appropriate installation file for your machine [from here](https://sourceforge.net/projects/turbovnc/files/3.0.2/)
- Log into EMBL cluster: `ssh username@login1.cluster.embl.de`
- In terminal window, type `vncserver`. This will display the following:
  ```
  Desktop 'TurboVNC: login02.cluster.embl.de:1 (user)' started on display login02.cluster.embl.de:#

  Starting applications specified in /home/user/.vnc/xstartup.turbovnc
  (Enabling VirtualGL)
  Log file is /home/user/.vnc/login2.cluster.embl.de:#.log

  For access from your client computer to this machine
  run "vncviewer login2.cluster.embl.de:##
  ```

  - where `##` could be any single of double digit number
- Then start `TurboVNC` on your local machine
- In the `VNC server` field of the GUI, enter `login2.cluster.embl.de:##` (replace `##` with the number displayed e.g. `login2.cluster.embl.de:17` ) and press `Connect` button
- Enter your EMBL username and password, This will bring you the following window
  - ![Turbo VNC Viewer](screenshots/turboVNC_1.png)
- Click on the top left of the `Activities` icon as indicated by the arrow in the following image
  - ![Activities](screenshots/turboVNC_2.png)
- Then click on the pad icon as shown by arrow in the image below
  - ![Pad](screenshots/turboVNC_3.png)
- Click on `Xfce Terminal` as indicated by arrow in the following image
  - ![Xfce Terminal](screenshots/turboVNC_4.png)
  - This will bring you to a bash terminal window [Bash](screenshots/turboVNC_5.png)
- In bash terminal window, type:
  - `source /g/cba/miniconda3/init_conda.sh`
  - `conda activate ${ENVIRONMENT_NAME}` e.g. `conda activate cellpose`
  - `module load PyQt5`
- To kill this vnc session, type `vncserver -kill :##` in the terminal window of HPC. _NOTE_: replace `##` with the number displayed
