# Using MoBIE for visual image analysis QC

MoBIE is a desktop tool for efficient visual QC of large image data sets.

- [I2K 2023 tutorial](...)
- [MoBIE publication](https://www.nature.com/articles/s41592-023-01776-4)
- [MoBIE documentation](https://mobie.github.io/)

## Use MoBIE on EMBL Jupyter Desktop

Advantages: MoBIE is preinstalled and the Jupyter Desktop has fast access to the EMBL group shares.

- [Log into Jupyter Desktop](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-embl-jupyterhub.md?ref_type=heads#image-analysis-on-embls-jupyter-desktop)
- To launch Fiji open a terminal window, type `fiji` and hit enter
- To update Fiji and MoBIE: `Help > Update...` and restart Fiji
- To use MoBIE in Fiji: `Plugins > MoBIE >` offers various entry points

## Use MoBIE on your personal computer

Advantages: You don't need to log into some other computer.

- [Install Fiji](https://fiji.sc/)
- [Add the MoBIE-beta update site](https://github.com/mobie/mobie-viewer-fiji/blob/main/MOBIE-BETA.md)
  - MoBIE-beta ships the latest development version, which may comprise features that you need
- To use MoBIE in Fiji: `Plugins > MoBIE >` offers various entry points
