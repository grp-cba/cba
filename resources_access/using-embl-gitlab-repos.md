# Using EMBL Gitlab repositories

[How to use **Gitlab** using command line git?](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

## Cloning EMBL Gitlab repo
- To be able to clone this repository you need have to [have GitLab SSH keys configured](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/setting-up-ssh-key.md?ref_type=heads#set-up-ssh-key-for-embl-gitlab). If you have trouble please ask for help in the Bio-IT [git chat channel](https://chat.embl.org/embl/channels/git).
- Once you have the SSH keys set up, execute the below command in the terminal window of your compute resource (e.g. [EMBL Jupyterhub](https://jupyterhub.embl.de/)).
- _Notes:_
    - The repos are cloned in home directory of the system you are using.
    - Cloning is just done once

### An example for cloning a *CBA* repo to your home directory
```
git clone git@git.embl.de:grp-cba/cba.git
```

## Updating repo
- Before running analysis, updating the repo is recommended to get the latest code
- Since repos are cloned in user home folder, one can open terminal window in the user home folder and navigate to the repo folder

### An example for updating a repo
```
cd example-repo
git pull
```
- _Notes:_
    - It is always recommended to do `git pull` before you make any changes to repo files/folders

## Editing/extending repo
- *Recommended practice*
  - [Create a separate Gitlab branch](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#switch-to-a-branch) to make local changes in a repo 
    - Example:  `git checkout -b example-branch`
- *Not recommended*
  - Do `git stash` to save local changes # only if you did not voluntarily added anything to the repository
- _Notes:_
    - If you only do `git pull` without `git stash` and you had some local changes it may result in an error.
