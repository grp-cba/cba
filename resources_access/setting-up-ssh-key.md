# Using shh keys for EMBL
[What is ssh key?](https://www.ssh.com/academy/ssh/openssh)

## Set up ssh key for EMBL Gitlab

1. **Action:** Open terminal window
    - _Important_: Make sure that you are in your user home directory
    - _Note:_ [Windows users should install Git Bash](https://www.educative.io/answers/how-to-install-git-bash-in-windows) and do this in `Git Bash` terminal
1. **Action:** Type  `ssh-keygen -t rsa -b 2048` and press `Enter`
1. Displayed text:
    - _Generating public/private rsa key pair._
    - _Enter file in which to save the key (/home/user/.ssh/id_rsa):_

1. **Action:** Leave empty and press `Enter`
1.	Displayed text:
    - _Enter passphrase (empty for no passphrase):_
1. **Action:** Leave empty and press `Enter` twice
1. **Action:**	Type `eval $(ssh-agent -s)` and press `Enter`
1. **Action:**	Type `ssh-add ~/.ssh/id_rsa` and press `Enter`
1. Displayed text:
    - _Identity added: ~/.ssh/id_rsa (user@system)_

1. **Action:** Type `cat ~/.ssh/id_rsa.pub` and press `Enter`
1. **Action:** Copy the content displayed on the terminal that starts with "_ssh-rsa …_"
1. **Action:** [Go to: SSH Keys · User Settings · GitLab (embl.de)](https://git.embl.de/-/user_settings/ssh_keys)
1. **Action:** Paste the content in the `Key` box and press `Add key` as shown in the figure below

![Paste the content here](screenshots/user_settings_ssh_gitlab.png)    
*Paste the content copied in Step-11 here in `Key` box and press `Add key` (indicated by green arrows))*

## Set up ssh key for EMBL Cluster

- Repeat Steps 1-11
- **Action:** Go to [https://pwtools.embl.de/sshkey](https://pwtools.embl.de/sshkey)
- **Action:** Paste the content in the `Add/Modify your EMBL SSH Public Key` box and press `Change SSH Public Key`
  - _Note:_ Changes might take several minutes to hours to come into effect
