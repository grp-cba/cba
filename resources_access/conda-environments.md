## Multi-user conda for usage on EMBL HPC

### Setting up the conda installation

In order to set up a shared conda installation for multiple users on Linux infrastructure, follow these steps:

- Login to the cluster
- cd to a temporary download folder
    - `cd /g/cba/tmp`
- Download the latest version of miniconda (see https://docs.conda.io/en/latest/miniconda.html), e.g.
    - `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`
- Set executable permissions: `chmod +x Miniconda3-latest-Linux-x86_64.sh`
- Execute the installation script: `./Miniconda3-latest-Linux-x86_64.sh`
- Agree to the license
- Choose the installation directory and start the installation, e.g., enter as the location
    - `/g/cba/miniconda3`
- After the installation is finished, choose **no** when asked "Do you wish to initialize Miniconda3 by running conda init?".
- This will end the installation and print out a few lines explaining how to activate the conda base environment.
- Copy the line `eval "$(/path/to/miniconda3/bin/conda shell.YOUR_SHELL_NAME hook)"` and paste it into a new file `init_conda.sh`, e.g.
    - Replace `YOUR_SHELL_NAME` with `bash` (assuming you and your users are using a bash shell; for zshell, replace with zsh, etc.).
    - `vim /g/cba/miniconda3/init_conda.sh`
    - and add: `eval "$(/g/cba/miniconda3/bin/conda shell.bash hook)"`
    - and save the file
    - `chmod +x init_conda.sh`
- Now, the conda base environment can be activated via running `source init_conda.sh`.
- Use it to set up the environments to make the applications available to your users.
- run `ls -la` to check that everyone has `r` and `x` rights in `/g/cba/miniconda3`
- and run `chmod -R g+w /g/cba/miniconda3` to give all cba members write access

### Adding new environments 

- Login to the cluster (see above)
- Activate conda: `source /g/cba/miniconda3/init_conda.sh`
- Now add a new environment, e.g.
    - `conda -n cellpose yolo --prefix /g/cba/miniconda3/envs/cellpose ...`

### Current environments

#### mobie_utils 

##### 04.12.2023 tischi

```
source /g/cba/miniconda3/init_conda.sh
conda create --prefix /g/cba/miniconda3/envs/mobie_utils -c conda-forge mobie_utils
```

#### ome_zarr_env

##### ??.10.2023 bugra

- help: https://github.com/Euro-BioImaging/BatchConvert

#### ultrack

https://github.com/royerlab/ultrack

##### 11.12.2023 tischi

```
source /g/cba/miniconda3/init_conda.sh
conda create --prefix /g/cba/miniconda3/envs/ultrack python=3.10
conda activate ultrack
pip install 'napari[all]' scikit-image tqdm tifffile matplotlib scipy edt fire
pip install git+https://github.com/royerlab/ultrack
python -m pip install gurobipy 
```

##### 19.02.2024 tischi


```
source /g/cba/miniconda3/init_conda.sh
conda create --prefix /g/cba/miniconda3/envs/ultrack_jupyter python=3.10 pip
conda activate ultrack_jupyter
pip install 'napari[all]' fire gurobipy git+https://github.com/royerlab/ultrack
```

more information https://support.gurobi.com/hc/en-us/articles/13443862111761-How-do-I-set-system-environment-variables-for-Gurobi-



