# Support statistics

## Quarterly Data Science Reports

1. [Reporting document](https://docs.google.com/document/d/1ESrwHBBVqUWHj7gfGdf8X9K0PCfkBGfABQ61NKISwSE/edit#heading=h.pw8vjgufs472)
1. Add new header to the document with the dates of the reporting period
1. Run stats python code.....
1. Add CBA stats to document
1. Look in Google Calendar and e-mails for courses in that period and add to document
     - Search terms: course, workshop, webinar, talk, speaker, hackathon
1. Look in Bio-IT calendar LINK for CBA courses
     - If you find courses that are not in the image-analysis-support google calendar add them there
1. Ping Julian and Ziqiang to add their statistics and publications
1. Ask Tischi for publications with CBA contribution in that period
1. Ask Tischi for any other news, e.g. new grants
1. When done, send e-mail to Nina Habermann (nina.habermann@embl.de) with a link to the heading of that time period in the document

## Projects

### View all projects

- follow this link to view all projects: https://git.embl.de/explore/projects?topic=cba,support,bioimage%20analysis

### View projects where specific tools were used

You can add more topics to the above search, e.g.

- cellpose: https://git.embl.de/explore/projects?topic=cba,support,bioimage%20analysis,cellpose
- nextflow & cellpose: https://git.embl.de/explore/projects?topic=cba,support,bioimage%20analysis,cellpose,nextflow

### Count active projects

It would be great to find a way to see on which projects we worked on during a certain period of time but I don't know how to achieve this.
