# requirements:
# pip install python-gitlab
# pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
# pip install beautifulsoup4
# tischi's mac: conda activate gitlab


import os
import os.path
import pickle
import re
from datetime import datetime as dt
from collections import Counter

import gitlab
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build


def remove_html_tags(text):
    """Remove html tags from a string"""
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


def get_calendar_service(client_secrets_file):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                client_secrets_file,
                SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('calendar', 'v3', credentials=creds)
    return service


def calendar_report(service, start_date, end_date, must_have_tags=None, either_have_tags=None,
                    calendar_name='image analysis support'):
    print('Getting the events...')
    events_result = service.events().list(
        calendarId=get_calendar_id(service, calendar_name),
        timeMin=start_date,
        timeMax=end_date,
        maxResults=2500,
        singleEvents=True,
        orderBy='startTime').execute()
    events = events_result.get('items', [])

    # Store event information, keyed by the event's recurring ID or unique ID
    event_info = {}

    for event in events:
        description = event.get('description', '')
        if must_have_tags is None or all(tag.lower() in description.lower() for tag in must_have_tags):
            if either_have_tags is None or any(tag.lower() in description.lower() for tag in either_have_tags):
                event_id = event.get('recurringEventId') or event.get('id')
                if event_id not in event_info:
                    event_info[event_id] = {
                        'name': event.get('summary', 'No Title'),
                        'dates': [],
                        'description': description,
                        'location': event.get('location', '')
                    }
                start = event.get('start', {}).get('date') or event.get('start', {}).get('dateTime')
                # Extract just the date part
                start_date = dt.fromisoformat(start).date()
                event_info[event_id]['dates'].append(str(start_date))

    print("")
    for info in event_info.values():
        print(f'{info["name"]}')
        # Prepend a dash to the dates line and remove the time part
        print(f'- Dates: {", ".join(date.split("T")[0] for date in info["dates"])}')
        if info['location']:
            print(f'- Location: {info["location"]}')
        # Remove HTML tags and correct the formatting
        description_text = remove_html_tags(info['description'])
        # Normalize white spaces and split into lines
        description_lines = ' '.join(description_text.split()).split('- ')
        # Join lines with a dash and a space, ensuring one dash per bullet
        description_with_bullets = '- ' + '\n- '.join(line.strip() for line in description_lines if line.strip())
        print(description_with_bullets + '\n')


def calendar_report_total_hours(service, start_date, end_date, title, calendar_name='image analysis support'):
    print('Getting the events...')
    events_result = service.events().list(
        calendarId=get_calendar_id(service, calendar_name),
        timeMin=start_date,
        timeMax=end_date,
        maxResults=2500,
        singleEvents=True,
        orderBy='startTime').execute()
    events = events_result.get('items', [])

    total_hours = 0

    for event in events:
        if event.get('summary', '') == title:
            start = event.get('start', {}).get('dateTime')
            end = event.get('end', {}).get('dateTime')
            if start and end:
                start_dt = dt.fromisoformat(start)
                end_dt = dt.fromisoformat(end)
                duration = (end_dt - start_dt).total_seconds() / 3600.0  # Convert seconds to hours
                total_hours += duration

    print(f"Total hours for events titled '{title}': {total_hours}")

    return total_hours


def get_calendar_id(service, calendar_name='image analysis support'):
    # Call the Calendar API to list all calendars
    calendar_list = service.calendarList().list().execute()

    # Find the calendar with the name 'image analysis support'
    for calendar_entry in calendar_list['items']:
        if calendar_entry['summary'] == calendar_name:
            calendar_id = calendar_entry['id']
            print(f"Calendar ID for {calendar_name}: {calendar_id}")
            return calendar_id
    else:
        print("Calendar not found.")
        exit()


def add_file_to_all_repos(group_name, tags, source_file_path, target_file_name):
    group_id = gitlab_group_ids[group_name]
    report_group = gl.groups.get(group_id)
    required_tags = set(tags)
    
    with open(source_file_path, 'r') as file:
        file_content = file.read()

    projects_list = report_group.projects.list(all=True)
    for group_project in projects_list:
        project = gl.projects.get(group_project.id)
        project_tags = set(project.tag_list)
        if not required_tags.issubset(project_tags):
            continue
        print(project.name)

        if True:
            try:
                project.files.create({
                    'file_path': target_file_name,
                    'branch': 'main',  # or 'master' depending on the branch you want to commit to
                    'content': file_content,
                    'commit_message': f'Add {target_file_name} file'
                })
                print(f"Added project-metadata.md to {project.name}")
            except gitlab.GitlabCreateError as e:
                print(f"Failed to add project-metadata.md to {project.name}: {e}")


def correct_typo_in_all_repos(group_name, tags, target_file_name, typo, correction):
    group_id = gitlab_group_ids[group_name]
    report_group = gl.groups.get(group_id)
    required_tags = set(tags)

    projects_list = report_group.projects.list(all=True)
    for group_project in projects_list:
        project = gl.projects.get(group_project.id)
        project_tags = set(project.tag_list)
        if not required_tags.issubset(project_tags):
            continue

        print(f"Checking {project.name} for typos in {target_file_name}")

        try:
            file = project.files.get(file_path=target_file_name, ref='main')
            file_content = file.decode().decode('utf-8')
            if typo in file_content:
                updated_content = file_content.replace(typo, correction)

                file.content = updated_content
                file.save(branch='main', commit_message=f'Fix typo in {target_file_name}: {typo} to {correction}')

                print(f"Corrected typo in {project.name}")
            else:
                print(f"No typo found in {project.name}")
        except gitlab.GitlabGetError:
            print(f"{target_file_name} not found in {project.name}")
        except gitlab.GitlabUpdateError as e:
            print(f"Failed to update {target_file_name} in {project.name}: {e}")


def consultancy_report(group_name, tags, start_date, end_date):
    group_id = gitlab_group_ids[group_name]
    report_group = gl.groups.get(group_id)
    required_tags = set(tags)

    # Convert string dates to datetime objects
    start_date_obj = dt.strptime(start_date, '%Y-%m-%d')
    end_date_obj = dt.strptime(end_date, '%Y-%m-%d')

    # Initialize
    support_issue_count = 0
    new_projects = []
    active_projects = {}
    checked_items_counter = Counter()

    # Count new and active projects with required tags
    projects_list = report_group.projects.list(all=True)
    for group_project in projects_list:
        project = gl.projects.get(group_project.id)
        project_tags = set(project.tag_list)
        if not required_tags.issubset(project_tags):
            continue
        project_creation_date = dt.strptime(project.created_at.split('T')[0], '%Y-%m-%d')
        if start_date_obj <= project_creation_date <= end_date_obj:
            new_projects.append(project.name)
        commits_since_start_date = project.commits.list(since=start_date_obj.isoformat() + 'Z', all=True)
        
        # Filter out commits with messages that were not related
        # to actually working on the project
        commits_since_start_date = [
            commit for commit in commits_since_start_date
            if "Fix typo in project_metadata.md" not in commit.message 
            and not "target_file_name" in commit.message
        ]
        
        if commits_since_start_date:
            last_commit_date = dt.strptime(commits_since_start_date[-1].created_at.split('T')[0], '%Y-%m-%d')
            if start_date_obj <= last_commit_date <= end_date_obj:
                active_projects[project.name] = len(commits_since_start_date)

        # Fetch the project-metadata.md file from the repository
        try:
            file_content = project.files.get(file_path='project_metadata.md', ref='main').decode().decode('utf-8')
            # Parse checked items
            checked_items = re.findall(r'- \[X\] ([^\n]+)', file_content)
            checked_items_counter.update(checked_items)
        except Exception as e:
            print(f"Failed to fetch or parse project_metadata.md from {project.name}: {e}")

    # Count issues in CBA support issues project
    issue_project = gl.projects.get('grp-cba/image-analysis-support')
    issues_list = issue_project.issues.list(all=True)
    for issue in issues_list:
        issue_creation_date = dt.strptime(issue.created_at.split('T')[0], '%Y-%m-%d')
        if start_date_obj <= issue_creation_date <= end_date_obj:
            support_issue_count += 1

    # Print results
    separator = "------------------------------------------"
    print(separator)
    print(f"Consultancy group: {group_name}")
    print(f"Total projects: {len(projects_list)}")
    if group_name == "CBA":
        print(f"Total consultancies: {len(issues_list)}")
    print(f"From {start_date} to {end_date}")
    print(f"  New projects: {len(new_projects)}")
    print(f"  Active projects: {len(active_projects)}")
    if group_name == "CBA":
        print(f"  New consultancies: {support_issue_count}")
    print(separator)

    if new_projects:
        print("New projects:")
        for project_name in new_projects:
            print(f"- {project_name}")

    if active_projects:
        print("Active project commits:")
        for project_name, commit_count in active_projects.items():
            print(f"- {project_name}: {commit_count}")
    print(separator)

    print("\nChecked Metadata Count:")
    for item, count in checked_items_counter.items():
        print(f"- {item}: {count}")
    print(separator)


# TODO
#  - [X] make the below a function with arguments: groups, tags, start_date, end_date
#  - [ ] add the courses from the GCal (code above) to the quarterly_report_cba
#  - [ ] actually add all courses from 2023 to the GCal, be careful with the repetition
#  - [ ] somehow capture whom we support (EMBL unit, site), probably a tag in the gitlab projects
#  - [ ] think about courses tags: course, internal, external, ...?
#  - [ ] add consultancies with to the GCal, tags: "consultancy" and ${gitlab repo name}
#  - [ ] parse the consultancies and make a project active if there was one
#  - [ ] code: first run the on gitlab and fetch list of all project names; this list can
#          be used to compare to the tags in the
#          gCal description to see which project the consultancy was about.


# Initialize Google Calendar access
# If modifying these SCOPES, delete the file token.pickle.
#SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']
#service = get_calendar_service('/Volumes/cba/statistics/client_secret_748311617705-aoqtjrm0foevgneqc8ecmfjgdlqfs8rt.apps.googleusercontent.com.json')

#calendar_report(service,
#                 "2022-10-01T00:00:00Z",
#                 "2024-03-30T00:00:00Z")

#calendar_report_total_hours(service, "2022-01-01T00:00:00Z", "2024-12-30T00:00:00Z", "Moritz Anavo")

#calendar_report(service,
                # "2023-03-12T00:00:00Z",
                # "2024-03-12T00:00:00Z",
                # ["Type:"],
                # ["Workshop", "Conference"])


gitlab_group_ids = {"CBA": 2166, "MIF": 7906, "IC": 2144}


if True:
    private_token = os.environ.get("GITLAB_PRIVATE_TOKEN")
    gl = gitlab.Gitlab('https://git.embl.de', private_token=private_token)
    consultancy_report("CBA",
                    ["bioimage analysis", "support"],
                    "2024-09-12",
                    "2024-11-06")

if False:
    consultancy_report("IC",
                    ["bioimage analysis", "support"],
                    "2023-04-08",
                    "2024-04-08")

if False:
    consultancy_report("MIF",
                    ["bioimage analysis", "support"],
                    "2023-04-08",
                    "2024-04-08")

#
# Danger zone
#

# private_token = os.environ.get("GITLAB_PRIVATE_FULL_ACCESS_TOKEN")
# gl = gitlab.Gitlab('https://git.embl.de', private_token=private_token)
# add_file_to_all_repos("CBA",
#                     ["bioimage analysis", "support"],
#                     "/Users/tischer/Documents/cba-support-template/project_metadata.md",
#                     "project_metadata.md", )

# private_token = os.environ.get("GITLAB_PRIVATE_FULL_ACCESS_TOKEN")
# gl = gitlab.Gitlab('https://git.embl.de', private_token=private_token)
# correct_typo_in_all_repos("CBA",
#                     ["bioimage analysis", "support"],
#                     "project_metadata.md",
#                      "Netflow",
#                      "Nextflow" )

#
# Experimental stuff
#

# support_projects = False
# cba_projects = False
# commits = False
# issues = False
#
# if commits:
#     # Sort projects by number of commits
#     # Define the group IDs
#     group_ids = [2166, 7906]  # cba, mif
#
#     # Prepare a list to hold project data
#     project_data = []
#
#     # Iterate over the group IDs
#     for group_id in group_ids:
#         # Get the group
#         group = gl.groups.get(group_id)
#
#         # List all projects in the group
#         projects = group.projects.list(all=True)
#
#         for project in projects:
#             # Get the project by ID
#             project = gl.projects.get(project.id)
#
#             # Count the commits since project creation
#             commits_since_creation = project.commits.list(since=project.created_at, all=True)
#             commit_count = len(commits_since_creation)
#
#             # Store project details and commit count
#             project_data.append({
#                 "name": project.name,
#                 "id": project.id,
#                 "commit_count": commit_count
#             })
#
#     # Sort project data by commit count in descending order
#     sorted_projects = sorted(project_data, key=lambda x: x['commit_count'], reverse=True)
#
#     # Print sorted project data
#     for project in sorted_projects:
#         print(f"Project Name: {project['name']}")
#         print(f"Project ID: {project['id']}")
#         print(f"Number of commits since project creation: {project['commit_count']}")
#
# if support_projects:
#     # Define the required tags
#     tags = {"bioimage analysis", "support", "nextflow"}
#
#     # Define the specific group IDs
#     group_ids = [2166, 7906]  # cba, mif
#
#     # List the specified groups by their IDs
#     groups = [gl.groups.get(group_id) for group_id in group_ids]
#
#     # Loop through each group and list projects
#     for group in groups:
#         project_count = 0
#         for project in group.projects.list(all=True):
#             project_creation_date = datetime.strptime(project.created_at.split('T')[0], '%Y-%m-%d')
#             project_tags = set(project.tag_list)
#             if project_creation_date > filter_date and tags.issubset(project_tags):
#                 project_count += 1
#         if project_count > 0:
#             print(f"\n{group.name} projects created after {date}: {project_count}")
#
# if cba_projects:
#     # Define the group, cba = 2166, mif = 7906
#     # group = gl.groups.get('grp-cba')
#     group = gl.groups.get(2166)
#
#     # List all projects in the group
#     print("\n# Projects created after", date)
#     project_count = 0
#     for group_project in group.projects.list(all=True):
#         project = gl.projects.get(group_project.id)
#         project_creation_date = datetime.strptime(project.created_at.split('T')[0], '%Y-%m-%d')
#         if project_creation_date > filter_date:
#             project_count = project_count + 1
#             print("\n------")
#             print(f"Total Project Count: {project_count}")
#             print(f"Name: {project.name}")
#             print(f"Creation: {project.created_at.split('T')[0]}")
#             print(f"Tags: {', '.join(project.tag_list) if project.tag_list else 'None'}")
#             commits_since_creation = project.commits.list(since=filter_date, all=True)
#             commit_count = len(commits_since_creation)
#             print(f"Commits: {commit_count}")
#             #  print(f"Description: {project.description}")
#
# if issues:
#     # Get the support issues project
#     issue_project = gl.projects.get('grp-cba/image-analysis-support')
#
#     # List all issues
#     print("\n# Issues created after", date)
#     issue_count = 0
#     for issue in issue_project.issues.list(all=True):
#         issue_creation_date = datetime.strptime(issue.created_at.split('T')[0], '%Y-%m-%d')
#         if issue_creation_date > filter_date:
#             issue_count = issue_count + 1
#             print("\n------")
#             print(f"Issue Count: {issue_count}")
#             print(f"Issue ID: {issue.id}")
#             print(f"Issue Title: {issue.title}")
#             print(f"Issue Creation: {issue.created_at.split('T')[0]}")
#             print(f"Issue Description: {issue.description}")
