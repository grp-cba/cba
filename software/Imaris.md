# Imaris

Imaris is a commerical user-friendly solution for the analysis of big volumetric multi-channel time-lapse image data.

## Install

The ALMF provides access to virtual machines with Imaris installations.
Please sent a mail to [almf@embl.de](mailto:almf@embl.de) to get access.

## Learn

We recommend watching the official [video tutorials](https://imaris.oxinst.com/learning/?businesses=bitplane&media=VideoTutorials).

In [ Imaris > Help ] there also is Quickstart Tutorial and other useful help.

## How-To

### Export Surface objects as a label mask

[Learn more about label masks](https://neubias.github.io/training-resources/connected_components/index.html)

Protocol:

- First, if you have more than 255 objects in any of the timepoints _and_ if your data type is 8-bit you need to change the data type to either 16-bit or float using [ Edit > Change data type...]
  - Be careful, if you have a big data set, because going from 8-bit to 16-bit will double the size of the data.
- Here's a [short video](https://www.dropbox.com/s/6c1tys27ipmuac3/Channel_mask_with_random_colors.mov?dl=0) summarising below steps
- In the Surface object select the Edit tab (pencil symbol).
- Within the Edit tab, click `Mask All..`.
  - [X] Duplicate channel before applying mask
  - [X] Random color map
  - [X] Apply to all time points
- Now you have a new "label mask" channel, visible in [ Edit > Show display adjustment ]
  - It will appear as a channel with a "funny" looking color map with many colors
- Save your project with the new channel [ File > Save ]
- If you open the IMS file, e.g. with Fiji, it will contain the label mask channels

### Only see Surface objects within one plane

TODO: Which the new version of Imaris this might now be the best way...

This is very useful for the case where one has very many objects that occlude each other.
For example during object classification where you need to assign objects to different classes.

Protocol:

- Uncheck all Volume objects (otherwise they may occlude the slice that you want to look at)
- Within the Surface object, check the Slicer View.
- Then you only see the surfaces in the current slice together with the intensity information.

## FAQ

#### What is meant by the 'Quality' filter in statistics?

The 'Quality' is the intensity at the center of the spot in the channel the spot was detected. If 'Background Subtraction' was not checked this is the intensity of the channel gaussian filtered by 3/4 of spot radius. If 'Background Subtraction' was active the intensity is the one of a Gaussian filtered channel from above minus the intensity of the original channel Gaussian filtered by 8/9 of spot radius.
