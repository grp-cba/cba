To use cellpose and cellpose plugin in napari with GPU support, go here:
[jupyterhub](http://jupyterhub.embl.de/)

- Use your EMBL credentials to log in
- In 'Server Options', select `Napari GPU Desktop` and press `Start`
- Open the `Terminal`, you will see `(base) username@jupyter-username:/home/username`
- Type: `conda activate napari-gpu` (This will activate the conda environment named "napari-gpu". Cellpose and napari are installed within this environment)
- In terminal, you will see now : `(napari-gpu) username@jupyter-username:/home/username` 
    - Type: `cellpose` to start cellpose GUI 
    - Type: `napari -w cellpose-napari` to start cellpose plugin in napari
    
