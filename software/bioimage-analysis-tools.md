[[_TOC_]]

# General information

## EMBL Jupyter Desktop

The image analysis "EMBL Jupyter Desktop" is a virtual desktop with decent hardware and several bioimage analysis tools preinstalled.

It supports GPU computing for deep learning applications.

All EMBL group shares are accessible.

The Jupyter Desktop is convenient for interactive analysis of few images.

Instructions and more details about the Jupyter Desktop can be found [here](resources_access/using-embl-jupyterhub.md).

## EMBL HPC

EMBL HPC refers to the EMBL's high performance computer cluster, which we recommend using for batch analysis.

## Nextflow

Nextflow is a workflow management system that we use for batch analysis on the EMBL high performance compute cluster.


# Tools

## CellProfiler

Recent versions of CellProfiler also offer 3D analysis capabilities (but this has not been throughly explored yet by us).
Building the workflows in the GUI requires some training and experience as it comes close to programming.

### Use it for

- Highly standardised 2D images, such as obtained with high-throughput microscopy

### How to use it

- You interact with a GUI to construct an image analysis workflow for one image set
- This workflow can then be readily applied from within the GUI to many images of the same kind

### Training material

- [CellProfiler website](https://cellprofiler.org/)
- [CellProfiler YouTube tutorial](https://www.youtube.com/watch?v=eriZdORpFxs)
- [CellProfiler example pipelines](https://cellprofiler.org/examples)

### Installations

- [Install on your own computer](https://cellprofiler.org/releases)
- [CellProfiler on EMBL Jupyter Desktop](resources_access/using-embl-jupyterhub.md/#launch-preinstalled-software)
    - Only one recent version is available
- [CellProfiler on EMBL HPC](https://git.embl.de/grp-almf/almf-cluster-scripts/-/blob/master/CellProfiler/HowtoCP_Cluster.md?ref_type=heads#generate-slurm-jobs)
    - Several CellProfiler versions are available

### Notes

- Recent versions of CellProfiler also support the analysis of 3D images, but this has not been thoroughly tested by us

## ilastik

ilastik offers machine learning for pixel and object classification, as well as tracking.
There are no coding skills required.

### Use it for

* Various image analysis tasks (semantic segmentation, object classification, boundary-based segmentation, neural networks, tracking) on data with up to 5 dimensions (3 spacial + time + channels).

### How to use it

- Train a machine learning classifier by annotating representative examples on one or multiple images in a GUI.
- The trained classifier can be applied in batch to more unseen data (can be either done with the GUI, or via terminal/fiji macros/nextflow).


### Training material

- [ilastik documentation][ilastik-docs]
- [Basic ilastik tutorial on youtube][ilastik-yt-base]
- [Advanced ilastik tutorial on youtube][ilastik-yt-adv]

### Installations

* [Install on your own computer][ilastik-dl]
* [ilastik on EMBL Jupyter Desktop][embl-jupyter]
* [ilastik on EMBL HPC][embl-cluster-ilastik]

### Related tools

- MoBIE visualisation of ilastik output, [tutorial on youtube][mobie-tut]
- ilastik has a [Fiji plugin][ilastik-fiji] that allows converting data to/from ilastik hdf5, and running pre-trained ilastik workflows from within Fiji.
- ilastik Pixel Classification is integrated as a [detector into TrackMate][trackmate-ilastik]
- ilastik Pixel Classification workflows can be run from Python: [example notebook][ilastik-nb]

[embl-cluster-ilastik]: https://git.embl.de/kutra/ilastik-scripts
[embl-jupyter]: ../resources_access/using-embl-jupyterhub.md/#launch-preinstalled-software
[ilastik-dl]: https://www.ilastik.org/download
[ilastik-docs]: https://www.ilastik.org/documentation/
[ilastik-fiji]: https://github.com/ilastik/ilastik4ij?tab=readme-ov-file#ilastik-imagej-modules
[ilastik-install]: https://www.ilastik.org/documentation/basics/installation
[ilastik-nb]: https://github.com/ilastik/ilastik/tree/main/notebooks/pixel_classification_api
[ilastik-yt-base]: https://youtu.be/F6KbJ487iiU?feature=shared
[ilastik-yt-adv]: https://youtu.be/_ValtSLeAr0?feature=shared
[mobie-tut]: https://youtu.be/xe3FlkJ0nfU?feature=shared
[trackmate-ilastik]: https://imagej.net/plugins/trackmate/detectors/trackmate-ilastik

## Fiji

Fiji (is just ImageJ) is a GUI that is great for image inspection and exploratory image analysis using various plugins.
There are also many ways for writing scripts within Fiji but unfortunately there is no real consensus, which of the scripting languages to use.

- [EMBL Jupyter Desktop](resources_access/using-embl-jupyterhub.md)
- Cluster executable: ??

### Related tools

- JIPipe??? 


## Python scripting

Python is an easy and widespread scripting language with many bioimage analysis and related (deep learning) libraries.

- ?
- ?

### Training material

- Regular Bio-IT courses (TODO somehow link to them?)
- ???


## TrackMate

TrackMate is a GUI for 2D and 3D tracking. 
It supports splitting and merging events and offers a convenient StarDist2D integration for 2D object segmentation.

### GUI execution

- Available within [Fiji](#fiji)

### Batch execution

- https://git.embl.de/grp-cba/cba/-/issues/15


## elastix

Image registration.

Typically requires minimal python coding or command line skills.

### GUI execution

- Fiji wrapper?

### Python execution

...

### Batch execution

...




