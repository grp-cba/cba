## Installation/running instructions

Requirements:
- Download miniconda installer according to your operating system from [here](https://docs.conda.io/en/latest/miniconda.html)
  - Choose the latest python version
  - Make sure that you have at least 300 MB of free disc space
- Run the installer, and in advanced options check the option to add it to the system PATH environment variable

Verify installation:
- Open terminal window and type `conda` to get info on miniconda
  - If you get the info, conda (i.e. python manager/environment manager and also a command line tool) is successfully installed
  - Type `python --version` in the terminal window to get the python version you have installed
  - Type `python` in the terminal window to start using python

Run python code:
- To run python code, one should create a managing environment to contain the packages that are required by the code
- To create an environment with a specific python version, type:
  - `conda create -n environment-name python=x.x`, e.g., `conda create -n mycode python=3.9`
  - `-n` argument is used for naming the environment
- To activate the environment(i.e. to get in that environment) type:
  - `conda activate environment-name`
- One can install packages inside this environment using:
  - `pip install package-name`, e.g., `pip install tifffile`
  - This `tifffile` package will only be installed in this environment
- To run your python file, type `python` in terminal window and navigate to code directory (i.e. using `cd` command) and then type:
  - `%run python-code-file.py`
- To deactivate the environment(i.e. to get out of that environment) type:
  - `conda.bat deactivate environment-name`
