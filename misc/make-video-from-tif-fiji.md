# Make and save video files from tiff in Fiji

- Open `Fiji`
- Go to `Help > Update`
- Click on `Manage update sites`, tick `FFMPEG` and close the window.
- Restart `Fiji`
- Open your `tif` file
- Go to `File > Save As > Movie(FFMPEG)` and click `Run`
- In file name of `Export via FFMPEG` dialog, replace `.mpg` with `.mov` and click `Save`
    - NOTE - `.mov` extension is used since `Gitlab` currently accepts only this format to upload movies.
- A dialog box will appear, here you can set `Framerate` and `Bitrate` and then click `OK`

  ![FFMPEG settings](../screenshots/ffmpeg-settings.png)

    - Using a lower `Framerate` makes the movie slower and vice versa
