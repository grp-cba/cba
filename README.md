# Centre for Bioimage Analysis (CBA)

The CBA supports EMBL scientists and visitors in the reproducible extraction of biophysically meaningful measurements from microscopy images of biological samples.

## Getting support

Please send an e-mail to [image-analysis-support@embl.de](mailto:image-analysis-support@embl.de) describing in a few sentences what your bioimage analysis project is about.

## Acknowledging support

CBA support is **free of charge** for EMBL internal purposes, however if we successfully help you and the work results in a publication please acknowledge us:

1. For extensive support with scientific contributions please consider a co-authorship.
2. For smaller contributions please acknowledge us: **We thank the Centre for Bioimage Analysis (CBA) at the European Molecular Biology Laboratory (EMBL) for support.** 

## Prepare for a consultancy appointment

### Prepare minimal example data

It will be very helpful if you [share some minimal example data](https://git.embl.de/grp-cba/cba/-/blob/master/image-data-preparation.md) with us already before the meeting.

### Prepare a minimal presentation

It would be nice if you could prepare a very short presentation with only four slides, following below suggestions. 

1. Scientific background 
  - What biological insights do you wish to obtain?
  - Why are you interested in quantifying your image data?
2. Sample preparation and image acquisition
  - What kind of sample do you image?
  - What kind of staining did you do?
  - Which microscope did you use? 
3. Example images
  - Ideally two images: treated and untreated with a visible difference of what should be measured
4. Technicalities
  - Do you already know which number(s) you would like to measure?
  - How many images do you want to analyse (10, 100, 1000, 10000, 100000, ...)?
  - How big are your individual images (MB, TB)?
  - Do you already have experience with some image analysis software?
  - Did you already try to analyse the data?

Best wishes and we are looking forward to meeting you soon!

## Further information

Depending on your request during the above consultancy session we may

- solve your issue directly.
- identify (and teach you how to use) a tool that you can use on your own to solve your issue.
- find that the data acquisition (microscopy modality) may have to be adapted. 
- establish a collabration.

## Collaborations

Often, a consultancy session results in a collaboration that

- lasts for weeks, months or even for the duration of your whole PhD or postdoc.
- comprise a significant amount of large image data.
- requires significant compute resources.
- entails the development of custom image analysis workflows.

Such collaborations therefore require a certain level of organisation as detailed below.

### Contact person

After the kick-of meeting one main contact will be assigned to you. Please mainly communicate with her/him, unless your project requires input from other people.

Please note that we are supporting many projects in parallel and thus cannot be in the driving seat of all of them. Thus, **it is your responsibility to manage the speed and direction of our collaboration**, using any of the below communication channels.

#### E-mails

Please send e-mails directly to your main contact person and only use the image-analysis-support@embl.de mailing list if you want to reach many/all of us.

#### EMBL chat

The EMBL chat is a great alternative to e-mails:.

- [Using the EMBL chat](https://grp-bio-it.embl-community.io/blog/posts/2021-01-29-mattermost-embl-chat/)
- [More info about the EMBL chat](https://grp-bio-it.embl-community.io/blog/posts/2023-02-07-embl-chat-channels/)

#### Follow-up meetings

Please always feel free to schedule follow-up meetings with us! 

Please be pro-active about this! As mentioned, we typically work on many projects in parallel and will thus not push any specific project.

Please note that such follow-up meetings should _not_ take place in the above consultancy slots but be arranged separately with your main consultant.

### Data and software privacy

**Our default setting is that all data and all software is accessible to everyone within EMBL!** 

If you wish more privacy it is your responsibility to let us know and we will try to find ways to accommodate this.

### Data management

Please note that any research project at EMBL needs a data management plan (DMP) as per EMBL IP 71.

If you need help with your DMP feel free to [contact Jean-Karim Heriche](mailto:heriche@embl.de)

Minimal requirements for collaborating with the us will be to

- identify an efficient and scalable way of data sharing (without data duplication) 
  - e.g. [using the DMA](https://wiki.embl.de/itspublic/Data_Management_App)
- organise all primary image data for our collaboration under one project directory
- develop, document and follow a meaningful, consistent and machine readable file and folder naming scheme
- provide disk space to store image analysis output

Further information on data management:

- [Data management guide](https://git.embl.de/grp-ellenberg/Ellenberg_SOP/-/wikis/data-management/data-management-guide)
- [More data management documents](https://cbbcs-01.embl.de/#dmp)
- [Data sharing and managing using the DMA](https://wiki.embl.de/itspublic/Data_Management_App)
- [Using STOCKS for image data management](https://stocks.embl.de/docs/)
- [Microscopy-BIDS](https://www.frontiersin.org/articles/10.3389/fnins.2022.871228/full)
- [NFDI4Plants data specification ](https://github.com/nfdi4plants/ARC-specification/)


### Project management

#### GitLab

We are heavily relying on EMBL GitLab for several aspects of project- and software-management.

For specific asynchronous discussions, we use GitLab's powerful **issue tracking** functionality.

GitLab (git) also is a great tool for **software management** and we typically put all code that we develop with you into an EMBL GitLab repository.

A GitLab repository has a README markdown file which we use to **document** the solutions that we develop with you.

Once your project is ready for **publication**, we can simply make the GitLab repository public in order to make everything accessile to the readers of your article.

- [CBA GitLab projects overview](https://git.embl.de/explore/projects/topics/cba)
- [GitLab issue tracking](https://docs.gitlab.com/ee/user/project/issues/)
- [Using EMBL GitLab]()
- [Software management using git (advanced)]()

## QC of automated image analysis

To analyse many images (in parallel) we develop fully automated workflows. This is great, but entails the danger that things may go wrong without being noticed. Image data is particularly tricky as images may contain subtle differences (maybe just one "hot pixel") that one cannot see by eye but still cause an automated analysis to fail.

**It is your responsibility to always critically inspect any output generated by an automated analysis workflow**. Please contact us if you have doubts how to do such quality control for your data.


## Software tools

There is a very large amount of software tools for (bio)image analysis, too many for us to support. We thus need to carefully pick a few service grade tools.

In our view, service grade software tools should be

- reusable for many projects
- accessible
  - thus we prefer free over commercial software
- interoperable
  - have standard inputs and outputs
- actively maintained
  - active on forum.image.sc
  - lively github repository
- versioned
- documented
  - publication
  - training material
- tried and tested by a large user base for several years
  - check on forum.image.sc

Currently, we mainly rely on the following tools:

- [CellProfiler](https://cellprofiler.org/)
  - [image.sc](https://forum.image.sc/tag/cellprofiler)
  - [github](https://github.com/CellProfiler/CellProfiler/issues)
- [Fiji](https://fiji.sc/)
  - [image.sc](https://forum.image.sc/tag/fiji)
  - [zulip](https://imagesc.zulipchat.com/#narrow/stream/327238-Fiji)
- [ilastik](https://www.ilastik.org/)
  - [image.sc](https://forum.image.sc/tag/ilastik)
  - [github](https://github.com/ilastik/ilastik/issues)
  - [CLI](https://www.ilastik.org/documentation/basics/headless.html)
- [Cellpose](https://github.com/mouseland/cellpose#--cellpose--)
  - [image.sc](https://forum.image.sc/tag/cellpose)
  - [github](https://github.com/MouseLand/cellpose/issues)
- [Nextflow](https://www.nextflow.io/)
  - [slack](https://app.slack.com/client/T03L6DM9G/C02T98A23U7)
  - [~~image.sc~~](https://forum.image.sc/tag/nextflow)
- [Imaris (commerical)](https://imaris.oxinst.com/products/imaris-for-cell-biologists)
  - [support e-mail](mailto:eusupport@bitplane.com)
  - [(image.sc)](https://forum.image.sc/tag/imaris)


More information:

- [A hitchhiker's guide through the bio-image analysis software universe](https://febs.onlinelibrary.wiley.com/doi/epdf/10.1002/1873-3468.14451)


## Compute infrastructure

Image data often is very big and thus cannot be analysed on your personal computer. In addition, keeping up-to-date installations of the required software tools can be very laborious. Thus, in collaboration with EMBL Bio-IT and ITS we provide compute infrastructure with pre-installed tools.

Currently, we support

- Several tools installed on [EMBL Jupyter Desktop](https://git.embl.de/grp-cba/cba/-/blob/master/hardware/using-jupyter-desktop.md#image-analysis-on-embls-jupyter-desktop)
- [CellProfiler installed on the EMBL compute cluster](https://git.embl.de/grp-almf/almf-cluster-scripts/-/blob/master/CellProfiler/HowtoCP_Cluster.md#running-cellprofiler-v220315318413-on-the-embl-cluster-with-slurm-mini-tutorial)

## Getting more help

While we try to help you as good as possible, we also encourage you to help yourself, and also we ourselves cannot know everything and thus frequently reach out for help.

We recommend the following bioimage analysis communication channels:

EMBL internal:

- [image-analysis-support@embl.de](mailto:image-analysis-support@embl.de)
- [EMBL Image Analysis chat](https://chat.embl.org/embl/channels/image-analysis)

External:

- [image.sc forum](https://forum.image.sc/)
  - for all purposes
- [image.sc zulip](https://imagesc.zulipchat.com/) 
  - for developers

## Publishing your image data and results

Please check out these [checklists for image publishing](https://quarep-limi.github.io/WG12_checklists_for_image_publishing/intro.html).
