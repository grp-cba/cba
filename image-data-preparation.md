# Image data preparation

Please be reminded that in general, independent of an eventual collaboration with the CBA, EMBL requires a **data management plan** for every scientific project. More details can be found [here](https://git.embl.de/grp-cba/cba#data-management). In case you don't yet have a data management plan, please find below some information about how to share a minimal example dataset with us, e.g. to be looked at in a consultancy session.

## Select a minimal set of representative examples

Select **two to three** (not more) images that ideally show a clear and biologically relevant difference.

Please organise those images into a file and folder structure similar to the one below:

- minimal-example-data
  - negative_control.tif
  - positive_control.tif
  - treated.tif
  - README.txt

As indicated please add a short README text file where you describe:
  - the difference is that you see between those images
  - minimal information about the different treatments
  - what staining(s) we look in each (fluorescence) channel

## Sharing your minimal example data

If the selected minimal data are less than a few tens of GB, you may copy them onto the `/cba/exchange` network share.

Everybody has access with their normal EMBL user account.

Please be aware that **everyone at EMBL can access this data**; if you want to share data more privately please contact us to discuss different options.



  
  

